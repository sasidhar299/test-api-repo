pipeline {

  agent any

    // changes for DEMO
    // define the required environment variables, which are used later in pipeline
    environment {
        AXWAY_API_CONFIG_FILE = "${env.WORKSPACE}/ApiCode/api-sys-petstoreci/api/'(IT-EDS)-Petstore-CI-System-API.v1.config'/config.json"
        AXWAY_API_DEFINITION_FILE = "${env.WORKSPACE}/ApiCode/api-sys-petstoreci/api/'(IT-EDS)-Petstore-CI-System-API.v1.json'"
        AXWAY_APIM_CLI_HOME = "${env.WORKSPACE}/APIMCLI/apim-cli-${env.APIMCLI_VERSION}"
        PROPERTY_FILE_LOC = "${env.WORKSPACE}/PropertyFiles/conf"
        APIGW_MAVEN_PLUGIN_HOME = "${env.WORKSPACE}/apigw-maven-plugin-1.1.0"
    }

  stages{

    // cleans the workspace before everyrun
    stage('CleanWorkspace') {
        steps {
            deleteDir()
        }
    }

    //Stage to checkout the bitbucket code  into ApiCode directory in workspace
    stage('Checkout API Source Code') {
        steps {
            sh 'mkdir -p ApiCode'
            dir("ApiCode")
            {
                echo 'Checking out code from Bit Bucket... @@@@@ NEW PIPELINE @@@@@'  
                git credentialsId: 'bitbucket-cred', url: 'https://sasidhar-awsdevops@bitbucket.org/sasidhar299/test-api-repo.git'
            }
        }
    }

    //Stage to Checkout property files into PropertyFiles directory in workspace
    stage('Checkout property files') {
        steps {
            sh 'mkdir -p PropertyFiles'
            dir("PropertyFiles")
            {
                echo 'Checking out property file from Bit Bucket...'  
                git credentialsId: 'bitbucket-cred', url: 'https://sasidhar-awsdevops@bitbucket.org/sasidhar299/test-properties.git'
            }
        }
    }
    //Stage to download APIMCLI and Maven Plugin and also to unzip into jenkins workspace
    stage('Download APIMCLI & Maven Plugin') {
        steps {
            sh 'mkdir -p APIMCLI'
            dir("APIMCLI")
            {
                echo 'Downloading APIMCLI from AXWAY github releases into APIMCLI directory...'  
                sh "wget -nc https://github.com/Axway-API-Management-Plus/apim-cli/releases/download/apimcli-${env.APIMCLI_VERSION}/axway-apimcli-${env.APIMCLI_VERSION}.tar.gz" 
                sh "ls -al"
                sh "tar -xvzf axway-apimcli-${env.APIMCLI_VERSION}.tar.gz"
            }
            //echo 'Downloading Maven tool from AXWAY github releases...' 
            //sh "wget -nc https://github.com/Axway-API-Management-Plus/apigw-maven-plugin/archive/v1.1.0.tar.gz"
            //sh "tar -xvzf v1.1.0.tar.gz"
        }
    }

    //Stage to copy property file from cloned repo to APIMCLI directory
    stage('copy property files') {
            steps {
                    sh "cp ${PROPERTY_FILE_LOC}/* ${AXWAY_APIM_CLI_HOME}/conf"
            }
        }

    //Stage to perform lint process using spectral
    stage('spectral lint') {
            steps {
                    echo "This step is used to validate the API definition using spectral...."
            }
        }

    //Stage to perform API deployment to APIManager api-env environment
    stage('execute apimcli') {
            steps {
                    echo "axway home      --> ${AXWAY_APIM_CLI_HOME}"
                    echo "config file     --> ${AXWAY_API_CONFIG_FILE}"
                    echo "definition file --> ${AXWAY_API_DEFINITION_FILE}"
                    withCredentials([usernamePassword(credentialsId: 'dev-apim-credentials',
                                passwordVariable: 'PASSWORD',
                                usernameVariable: 'USERNAME')]) {
                    sh "${AXWAY_APIM_CLI_HOME}/scripts/apim.sh api import -s api-env -c ${AXWAY_API_CONFIG_FILE} -a ${AXWAY_API_DEFINITION_FILE} -u $USERNAME -p $PASSWORD"
                    }
            }
        }
    }
}